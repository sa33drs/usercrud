@extends('layouts.frontend')

@section('content')
    <div class="content my-4">
        <div class="container">
            <div class="heading text-center my-4">
                <h1>Create User</h1>
            </div>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form action="{{route('users.store')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="row mb-3">
                    <div class="col">
                        <input type="text" name="firstname" class="form-control" placeholder="First name" value="{{ old('firstname') }}">
                    </div>
                    <div class="col">
                        <input type="text" name="lastname" class="form-control" placeholder="Last name" value="{{ old('lastname') }}">
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col">
                        <input type="text" name="city" class="form-control" placeholder="City" value="{{ old('city') }}">
                    </div>
                    <div class="col">
                        <input type="text" name="age" class="form-control" placeholder="Age" value="{{ old('age') }}">
                    </div>
                </div>
                <div class="mb-3">
                    <label for="formFile" class="form-label">Image</label>
                    <input name="image" class="form-control" type="file" id="formFile">
                </div>
                <button type="submit" class="btn btn-primary w-100">Create</button>
            </form>
        </div>
    </div>
@endsection
