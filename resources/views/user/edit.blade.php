@extends('layouts.frontend')

@section('content')
    <div class="content my-4">
        <div class="container">
            <div class="heading text-center my-4">
                <h1>Update User</h1>
            </div>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form action="{{route('users.update', $user->id)}}" method="post" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="row mb-3">
                    <div class="col">
                        <input type="text" name="firstname" class="form-control" placeholder="First name"
                               value="{{ old('firstname') ?: $user->firstname }}">
                    </div>
                    <div class="col">
                        <input type="text" name="lastname" class="form-control" placeholder="Last name"
                               value="{{ old('lastname') ?: $user->lastname }}">
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col">
                        <input type="text" name="city" class="form-control" placeholder="City"
                               value="{{ old('city') ?: $user->city }}">
                    </div>
                    <div class="col">
                        <input type="text" name="age" class="form-control" placeholder="Age"
                               value="{{ old('age') ?: $user->age }}">
                    </div>
                </div>
                <img height="100" width="300" src="{{$user->imagePath}}" alt="" class="img-thumbnail mb-3">
                <div class="mb-3">
                    <label for="formFile" class="form-label">To change image upload new one</label>
                    <input name="image" class="form-control" type="file" id="formFile">
                </div>
                <button type="submit" class="btn btn-primary w-100">update</button>
            </form>
        </div>
    </div>
@endsection
