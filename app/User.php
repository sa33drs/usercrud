<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $fillable = [
        'firstname' , 'lastname' , 'image' , 'city' , 'age'
    ];

    public function getImagePathAttribute(){
        return '/image/users/' . $this->image;
    }
}
