<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class UserController extends Controller
{
    public function index()
    {
        $users = User::all();
        return  view('user.index',compact('users'));
    }


    public function create()
    {
        return view('user.create');
    }

    public function store(Request $request)
    {
        $request->validate(array_merge($this->rules(),['image' => 'required | image']));
        $user = new User();
        $user->firstname = $request->firstname;
        $user->lastname = $request->lastname;
        $user->city = $request->city;
        $user->age = $request->age;

        if ($request->hasFile('image')){
            $image = $request->file('image');
            $filename = time() . '.' . $image->getClientOriginalExtension();
            $image->storeAs('/image/users', $filename,'public');
            $user->image = $filename;
        }
        $user->save();
        return redirect()->route('users.index')->with('success','User created successfully');
    }


    public function edit(User $user)
    {
        return view('user.edit',compact('user'));
    }


    public function update(Request $request, User $user)
    {

        $request->validate($this->rules());
        $user->firstname = $request->firstname;
        $user->lastname = $request->lastname;
        $user->city = $request->city;
        $user->age = $request->age;

        if ($request->hasFile('image')){
            $image = $request->file('image');
            $filename = time() . '.' . $image->getClientOriginalExtension();
            $image->storeAs('/image/users', $filename,'public');
            $user->image = $filename;
        }
        $user->save();
        return redirect()->route('users.index')->with('success','User updated successfully');
    }


    public function destroy(User $user)
    {
        $completeImagePath = public_path() . $user->imagePath;
        if (File::exists($completeImagePath)){
            File::delete($completeImagePath);
        }
        $user->delete();
        return redirect()->route('users.index')->with('success','User deleted successfully');
    }

    private function rules(){
        $rules = [
            'firstname' => 'required | max:255',
            'lastname' => 'required | max:255',
            'city' => 'required | max:255',
            'age' => 'required | integer | lt:200',
        ];
        return $rules;
    }
}
